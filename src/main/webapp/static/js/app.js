angular.module('TaskApp', [
  'TaskApp.services',
  'TaskApp.controllers',
  'ngRoute'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/', {templateUrl: 'partials/home.html', controller: 'tasksController'})
    .when('/tasks', {templateUrl: 'partials/tasks.html', controller: 'tasksController'})
    .when('/tasks/update/:id', {templateUrl: 'partials/edit.html', controller: 'tasksDetailUpdateController'})
    .when('/tasks/add', {templateUrl: 'partials/add.html', controller: 'tasksController'})
    .when('/tasks/show/:id', { templateUrl: 'partials/show_task.html',controller: 'tasksDetailUpdateController'})
    .otherwise({redirectTo: '/tasks'});
}]);