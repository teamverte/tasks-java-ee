angular.module('TaskApp.controllers', [])

        /* Task controller */

        .controller('tasksController', ['$scope', 'taskAPIservice', function ($scope, taskAPIservice) {

                // Pour manipuler plus simplement les tasks au sein du contrôleur
                // On initialise les tasks avec un tableau vide : []
                //var tasks = $scope.tasks = [{ "title":"40","description":"tm"},{ "title":"40","description":"tm"}];
                $scope.taskAddUpdate = {id: null, title: "", description: ""};
                var task = {id: null, title: 'belh', description: ''};
                $scope.task = {id: null, title: '', description: ''};
                //Test
                $scope.tasks = [
                    {
                        title: 'Big the bigman',
                        description: "Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. \n\
                            Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, \n\
                            quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre \n\
                            spécimen de polices de texte."
                    },
                    {
                        title: 'test',
                        description: 'bof'
                    }
                ];



                //Version avec webservice
                /* 
                 $scope.tasks = [];
                 taskAPIservice.getTasks().success(function (response) {
                 //Digging into the response to get the relevant data
                 $scope.tasks = response.MRData.StandingsTable.StandingsLists[0].DriverStandings;
                 });
                 */


                // Ajouter un task
                $scope.createTask = function (task) {
                    taskAPIservice.createTask(task)
                            .then(
                                    $scope.getTasks,
                                    function (errResponse) {
                                        console.error('Error while creating Task.');
                                    }
                            );
                };

                $scope.updateTask = function (task, id) {
                    taskAPIservice.updateTask(task, id)
                            .then(
                                    $scope.getTasks,
                                    function (errResponse) {
                                        console.error('Error while updating Task.');
                                    }
                            );
                };

                $scope.submit = function () {
                    if ($scope.task.id === null) {
                        console.log('Saving New Task', $scope.task);
                        $scope.createTask($scope.task);
                    } else {
                        $scope.updateTask($scope.taskAddUpdate, $scope.taskAddUpdate.id);
                        console.log('Task updated with id ', $scope.taskAddUpdate.id);
                    }
                    //self.reset();
                };

//fin test
//
                // Enlever un task
                $scope.deleteTask = function (task) {
                    taskAPIservice.deleteTask(task).then(
                            function (d) {
                                console.log($scope.task);
                            },
                            function (errResponse) {
                                console.error('Error while deleting Task.');
                            }
                    );
                };

                // Cocher / Décocher tous les tasks
                $scope.markAll = function (completed) {
                    tasks.forEach(function (task) {
                        task.completed = !completed;
                    });
                };

                $scope.getTasks = function () {
                    taskAPIservice.getTasks().then(
                            function (d) {
                                $scope.tasks = d.data;
                                console.log(d.data);
                            },
                            function (errResponse) {
                                console.error('Error while fetching Currencies');
                            }
                    );
                };



                $scope.getTasks();


                // Enlever tous les tasks cochés
                $scope.clearCompletedTasks = function () {
                    $scope.tasks = tasks = tasks.filter(function (task) {
                        return !task.completed;
                    });
                };


            }


        ])
        .controller('tasksDetailUpdateController', ['$scope', 'taskAPIservice', '$routeParams', function ($scope, taskAPIservice, $routeParams) {

                var id = $routeParams.id;


                $scope.getTask = function (id) {
                    taskAPIservice.getTask(id).then(
                            function (d) {
                                $scope.task = d;
                                console.log($scope.task);
                            },
                            function (errResponse) {
                                console.error('Error while fetching Currencies');
                            }
                    );
                };
                $scope.updateTask = function (task, id) {
                    taskAPIservice.updateTask(task, id).then(
                            function (d) {
                                console.log($scope.task);
                            },
                            function (errResponse) {
                                console.error('Error while updating Task.');
                            }
                    );
                };
                $scope.submit = function () {
                    $scope.updateTask($scope.task, $scope.task.id);
                    console.log('Task updated with id ', $scope.task.title);

                    //self.reset();
                };
                $scope.getTask(id);

            }]);