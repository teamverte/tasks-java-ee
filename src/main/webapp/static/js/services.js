angular.module('TaskApp.services', [])
        .service('taskAPIservice', function ($http, $q, $location) {

            var taskAPIresult = {};
            var baseUrl = $location.absUrl().split('#')[0];
            taskAPIresult.getTasks = function () {
                console.log(baseUrl);
                return $http({
                    method: 'GET',
                    url: baseUrl + '/service/tasks'
                });
            }




            taskAPIresult.createTask = function (task) {
                return $http.post(baseUrl + '/service/tasks/add/', task
                        ).then(
                        function (response) {
                            return response.data;
                        },
                        function (errResponse) {
                            console.error('Error while creating user');
                            return $q.reject(errResponse);
                        }
                )
            };

            taskAPIresult.deleteTask = function (task) {
                return $http.post(baseUrl + '/service/tasks/delete/', task
                        ).then(
                        function (response) {
                            console.log("Task deleted");
                        },
                        function (errResponse) {
                            console.error('Error while deleting user');
                            return $q.reject(errResponse);
                        }
                )
            };

            taskAPIresult.updateTask = function (task) {
                return $http.post(baseUrl + '/service/tasks/update/', task
                        ).then(
                        function (response) {
                            return response.data;
                        },
                        function (errResponse) {
                            console.error('Error while updating user');
                            return $q.reject(errResponse);
                        }
                )
            };
            taskAPIresult.getTask = function (id) {
                return $http.get(baseUrl + '/service/tasks/show/' + id
                        ).then(
                        function (response) {
                            return response.data;
                        },
                        function (errResponse) {
                            console.error('Error while fetching user');
                            return $q.reject(errResponse);
                        }
                );
            };

            return taskAPIresult;
        });



/*['$http', '$q', function($http, $q){
 
 return {
 
 fetchAllUsers: function() {
 return $http.get('http://localhost:8084/laravel/service/tasks')
 .then(
 function(response){
 return response.data;
 }, 
 function(errResponse){
 console.error('Error while fetching users');
 return $q.reject(errResponse);
 }
 );
 }
 };
 
 }]);
 */
