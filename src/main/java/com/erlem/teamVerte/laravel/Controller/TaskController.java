package com.erlem.teamVerte.laravel.Controller;

import com.erlem.teamVerte.laravel.service.TaskService;
import com.laravel.teamVerte.Model.Task;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * TaskController C'est la partie "Controleur" de l'application. C'est dans
 * cette classe que s'effectue toute la gestion des t�ches Cette classe est en
 * lien direct avec l'application et les services.
 *
 * @author Thomas Lopes
 */
@CrossOrigin
@RestController
public class TaskController {

    private TaskService taskService;
    private static final Logger LOGGER = Logger.getLogger(TaskController.class.getName());

    /**
     * setTaskService Instancie le taskservice
     *
     * @param ts la variable qui modifie l'attribut "taskservice"
     */
    @Autowired(required = true)
    @Qualifier(value = "taskService")
    public void setTaskService(TaskService ts) {
        this.taskService = ts;
    }

    /**
     * getAllTask Cette m�thode retourne la liste de toutes les t�ches � l'aide
     * de la m�thode "getAllTasks".
     *
     * @return la variable qui modifie l'attribut "taskservice"
     */
    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public List<Task> getAllTasks() {
        return taskService.findAllTasks();
    }

    /**
     * getTask Cette m�thode permet de r�cup�rer une t�che. Plusieurs contr�les
     * sont effectu�s : - Si la variable locale task est null, alors un message
     * sur l'appli s'affiche "task with id xx not found". - sinon la t�che est
     * ex�cut�e dans l'application.
     *
     * @param id Ce param�tre est r�cup�r� directement dans l'url
     * (@RequestMapping) par la m�thode GET. Il sert � retrouver le task par
     * l'id (m�thode findById)
     * @return La variable qui modifie l'attribut "taskservice"
     */
    @RequestMapping(value = "/tasks/show/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> getTask(@PathVariable("id") int id) {
        LOGGER.info("Fetching task with id " + id);
        Task task = taskService.findById(id);
        if (task == null) {
            LOGGER.info("task with id " + id + " not found");
            return new ResponseEntity<Task>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Task>(task, HttpStatus.OK);
    }

    /**
     * createTask Cette m�thode cr�e une task et redirige vers la page de cette
     * task Plusieurs contr�les sont effectu�s : - Si la task existe d�j� on
     * retourne un message de conflict sur l'application - Sinon la task est
     * ajout� � l'aide de la m�thode addTask
     *
     * @param task Ce param�tre de type Task contient toute les informations du
     * formulaire de cr�ation de task.
     * @param ucBuilder Ce param�tre g�re la redirection de la page.
     * @return Un statut est retourn� (CREATED ou CONFLICT)
     */
    @RequestMapping(value = "/tasks/add/", method = RequestMethod.POST)
    public ResponseEntity<Void> createTask(@RequestBody Task task, UriComponentsBuilder ucBuilder) {
       LOGGER.info("Creating task " + task.getTitle());

        if (taskService.isTaskExist(task)) {
            LOGGER.info("A task with name " + task.getTitle() + " already exist");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }

        taskService.addTask(task);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/tasks/show/{id}").buildAndExpand(task.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    /**
     * updateTask Cette m�thode met � jour une task et redirige vers la page de
     * cette task
     *
     * @param task Ce param�tre de type Task contient toute les informations du
     * formulaire de modification de task.
     * @param ucBuilder Ce param�tre g�re la redirection de la page.
     * @return Un statut est retourn� lorsque la task est updat�.
     */
    @RequestMapping(value = "/tasks/update", method = RequestMethod.POST)
    public ResponseEntity<Void> updateTask(@RequestBody Task task, UriComponentsBuilder ucBuilder) {
        LOGGER.info("updating task " + task.getTitle());

        taskService.updateTask(task);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/tasks").buildAndExpand(task.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    /**
     * deleteTask Cette m�thode supprime une task et redirige vers la page de
     * cette task
     *
     * @param task Ce param�tre de type Task contient toute les informations de
     * la task.
     * @param ucBuilder Ce param�tre g�re la redirection de la page.
     * @return Un statut est retourn� lorsque la task est supprim�e.
     */
    @RequestMapping(value = "/tasks/delete", method = RequestMethod.POST)
    public ResponseEntity<Void> deleteTask(@RequestBody Task task, UriComponentsBuilder ucBuilder) {
        LOGGER.info("Deleting task " + task.getTitle());

        taskService.deleteTaskById(task.getId());

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/tasks").buildAndExpand(task.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

}
