/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erlem.teamVerte.laravel.DAO;

import com.laravel.teamVerte.Model.Task;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * TaskDAOImpl
 * Elle appartient � la partie "Mod�le" de l'application. 
 * Cette classe est l'impl�mentation de l'interface TaskDAO. Cette classe est en lien direct avec la base de donn�es. Elle ex�cute les demandes envoy�s par les m�thodes de la classe TaskController
 * @author Thomas Lopes
 */
@Repository
@Transactional
public class TaskDAOImpl implements TaskDAO {

    private SessionFactory sessionFactory;

    /**
        * setSessionFactory
        * Instancie la session
        * @param    sf   la variable qui modifie l'attribut "sessionFactory"
    */
    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    /**
        * findById
        * Cette m�thode retourne une task recherch�e via l'id
        * @param    id  l'identifiant qui permet de retrouver l'id dans la base de donn�es
        * @return       le task recherch� par l'id est retourn�
    */
    public Task findById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Task task = (Task) session.get(Task.class, new Integer(id));
        return task;
    }

    /**
        * addTask
        * Cette m�thode ajoute une task dans la base de donn�es
        * @param    task    la task a ajout� dans la base de donn�es
    */
    public void addTask(Task task) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(task);
    }

    /**
        * updateTask
        * Cette m�thode met � jour une task dans la base de donn�es
        * @param    task    la task a updat� dans la base de donn�es
    */
    public void updateTask(Task task) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(task);
    }

    /**
        * deleteTaskById
        * Cette m�thode supprime une task recherch�e via l'id
        * @param    id  l'identifiant qui permet de retrouver l'id dans la base de donn�es
    */
    public void deleteTaskById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Task task = (Task) session.get(Task.class, new Integer(id));
        session.delete(task);
        //This makes the pending delete to be done
        session.flush();
    }

    /**
        * findAllTasks
        * Cette m�thode retourne une liste de toutes les t�ches
        * @return    la liste des t�ches dans la base de donn�es
    */
    public List<Task> findAllTasks() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Task> tasksList = session.createQuery("from Task").list();
        /* for(Task t : tasksList){
            logger.info("Person List::"+p);
        }*/
        return tasksList;
    }

    /**
        * isTaskExist
        * Cette m�thode v�rifie si la t�che est existante dans la base de donn�es
        * @param    task    la task � v�rifier 
        * @return   TRUE si la task existe, FALSE sinon
    */
    public boolean isTaskExist(Task task) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.get(Task.class,task.getId()) != null;      
    }

}
