/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erlem.teamVerte.laravel.DAO;

import com.laravel.teamVerte.Model.Task;
import java.util.List;
import org.hibernate.SessionFactory;

/**
 * TaskDAO est l'interface pour implémenter le DAO.
 * @author Thomas Lopes
 */
public interface TaskDAO {
    
    void setSessionFactory(SessionFactory sf);
    Task findById(int id);
    void addTask(Task task);
     
    void updateTask(Task task);
     
    void deleteTaskById(int id);
 
    List<Task> findAllTasks(); 
          
    public boolean isTaskExist(Task task);
}
