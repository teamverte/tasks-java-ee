/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erlem.teamVerte.laravel.service;

import com.erlem.teamVerte.laravel.DAO.TaskDAO;
import com.laravel.teamVerte.Model.Task;
import java.util.List;

/**
 * TaskServiceImpl
 * Elle appartient � la partie "Modele" de l'application. 
 * C'est la classe fait le lien entre le controleur et le DAO est l'impl�mentation de l'interface TaskService.
 * @author Thomas Lopes
 */

public class TaskServiceImpl implements TaskService{

     private TaskDAO taskDAO;
     
     
    /**
        * setTaskDAO
        * Instancie le taskDAO
        * @param    td   la variable qui modifie l'attribut "taskDAO"
    */ 
    public void setTaskDAO(TaskDAO td){
         this.taskDAO=td;
     }

    /**
        * findById
        * envoie une requete � la bdd pour rechercher par id
        * @param    id   la variable qui permet la recherche de la task 
        * @return        la tache recherch�e par id
    */ 
    public Task findById(int id) {
        Task t = taskDAO.findById(id);
        
        return t;
    }

    /**
        * addTask
        * envoie une requete � la bdd pour ajouter une task
        * @param    task   la variable qui contient la task � ajouter
    */    
    public void addTask(Task task) {
        taskDAO.addTask(task);
    }

    /**
        * updateTask
        * envoie une requete � la bdd pour update une task
        * @param    task   la variable qui contient la task � modifier
    */     
    public void updateTask(Task task) {
        taskDAO.updateTask(task);
    }

    /**
        * deleteTaskById
        * envoie une requete � la bdd pour supprimer une task
        * @param    id   la variable qui contient l'id du task � supprimer
    */     
    public void deleteTaskById(int id) {
        taskDAO.deleteTaskById(id);
    }

    /**
        * findAllTasks
        * envoie une requete � la bdd pour retourner la liste de toutes les task
        * @return      la liste de toutes les tasks dans la bdd
    */
    public List<Task> findAllTasks() {
       return taskDAO.findAllTasks();
    }

    /**
        * isTaskExist
        * envoie une requete � la bdd pour valider si une task existe
        * @param       task     La variable qui contient la task � v�rifier si elle existe
        * @return               TRUE si la task existe FALSE sinon
    */
    public boolean isTaskExist(Task task) {
        return taskDAO.isTaskExist(task);
    }
}
