/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erlem.teamVerte.laravel.service;

import com.erlem.teamVerte.laravel.DAO.TaskDAO;
import com.laravel.teamVerte.Model.Task;
import java.util.List;

/**
 * TaskService
 * C'est l'interface pour implémenter le TaskService.
 * @author Thomas Lopes
 */

public interface TaskService {
    
    void setTaskDAO(TaskDAO td);
    Task findById(int id);
    void addTask(Task task);
     
    void updateTask(Task task);
     
    void deleteTaskById(int id);
 
    List<Task> findAllTasks(); 
     
    public boolean isTaskExist(Task task);
}
